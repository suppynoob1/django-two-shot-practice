from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account


# Create your views here.
def receipt_list(request):
    receipts = Receipt.objects.all()
    context = {
        "receipts": receipts,
    }
    return render(request, "receipts/list.html", context)


# def create_receipt(request):
#     if request.method == "POST":
#         form = ReceiptForm(request.POST)
#         if form.is_valid():
#             receipt = form.save(False)
#             receipt.purchaser = request.user
#             receipt.save()
#             return redirect("home")
#     else:
#         form = ReceiptForm()
#     context = {
#         "form": form,
#     }
#     return render(request, "receipts/create.html", context)


# def account_list(request):
#     accounts = Account.objects.filter(owner=request.user)
#     context = {
#         "accounts": accounts,
#     }
#     return render(request, "accounts/list.html", context)
